ansible_yaros
---------------------------------------------------

# ansible 2.8.3
# python version = 2.7.15rc1 (default, Nov 12 2018, 14:31:15) [GCC 7.3.0]
# Run ssh-add ssh.com before to add pub key from ansible server into remote server in authorized key
# Archivos de configuración para cambiar por analista
- EL ARCHIVO ODOO12-SERVER.CONF -- DEBEN CAMBIARSE SOLO PARA PWD O USUARIOS
- LOS ARCHIVOS "repos_odoo.txt" Y "repos_yaros.txt" DEBEN CONTENER LA LISTA DE URL PARA LOS REPOS
- EL ARCHIVO "list-upadte.txt" DEBE CONTENER LA LISTA DE MÓDULOS A ACTULIZAR 
- EL ARCHIVO "list-install.txt" DEBE CONTENER LA LISTA DE MÓDULOS A INSTALAR

Remember before test connection with server 
If you doesnt have connection you must be run ssh-add.sh script to add id_rsa.pub from ansible server into remote server


# how to run 
- ANSIBLE_CONFIG=$HOME/ansible_tasks_config/ansible.cfg ANSIBLE_ROLES_PATH=$HOME/ansible_tasks_config/roles/ /usr/local/bin/ansible-playbook $HOME/ansible_tasks_config/pullodoo12_playbook.yml -i $HOME/ansible_yaros/inventory/inventory.txt
- ANSIBLE_CONFIG=$HOME/ansible_tasks_config/ansible.cfg ANSIBLE_ROLES_PATH=$HOME/ansible_tasks_config/roles/ /usr/local/bin/ansible-playbook $HOME/ansible_tasks_config/update-module_playbook.yml -i $HOME/ansible_tasks_config/inventory/inventory.txt
## nota 
- Ansible lee links simbólicos y si los archivos a copiar ya existen me parece que no los copia , revisar eso, tal vez en los playbooks le configure para que conserver . no le recuerdo xd

