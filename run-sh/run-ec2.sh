#!/bin/bash
ANSIBLE_CONFIG=$HOME/ansible_tasks_config/ansible.cfg ANSIBLE_ROLES_PATH=$HOME/ansible_tasks_config/roles/ /usr/local/bin/ansible-playbook $HOME/ansible_tasks_config/ec2-status_playbook.yml -i $HOME/ansible_tasks_config/inventory/inventory.txt -e 'ansible_python_interpreter=/usr/bin/python3.6' >> $HOME/supervisorctl.log
