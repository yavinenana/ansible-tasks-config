#!/bin/bash
ANSIBLE_CONFIG=$HOME/ansible_tasks_config/ansible.cfg ANSIBLE_ROLES_PATH=$HOME/ansible_tasks_config/roles/ /usr/local/bin/ansible-playbook $HOME/ansible_tasks_config/backups-to-s3.yml -i $HOME/ansible_tasks_config/inventory/inventory.txt
ANSIBLE_CONFIG=$HOME/ansible_tasks_config/ansible.cfg ANSIBLE_ROLES_PATH=$HOME/ansible_tasks_config/roles/ /usr/local/bin/ansible-playbook $HOME/ansible_tasks_config/letsencrypt-ssl.yml -i $HOME/ansible_tasks_config/inventory/inventory.txt
ANSIBLE_CONFIG=$HOME/ansible_tasks_config/ansible.cfg ANSIBLE_ROLES_PATH=$HOME/ansible_tasks_config/roles/ /usr/local/bin/ansible-playbook $HOME/ansible_tasks_config/tools_playbook.yml -i $HOME/ansible_tasks_config/inventory/inventory.txt
ANSIBLE_CONFIG=$HOME/ansible_tasks_config/ansible.cfg ANSIBLE_ROLES_PATH=$HOME/ansible_tasks_config/roles/ /usr/local/bin/ansible-playbook $HOME/ansible_tasks_config/libraries_playbook.yml -i $HOME/ansible_tasks_config/inventory/inventory.txt

# sed -i 's/odoo12prod-ee,odoo12prod-ce/henry.yaroscloud.com/g' backups-to-s3.yml letsencrypt-ssl.yml libraries_playbook.yml sysstat_playbook.yml
# sed -i 's/henry.yaroscloud.com/odoo12prod-ee,odoo12prod-ce/g' backups-to-s3.yml letsencrypt-ssl.yml libraries_playbook.yml sysstat_playbook.yml
