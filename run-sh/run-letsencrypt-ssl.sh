#!/bin/bash
ANSIBLE_CONFIG=$HOME/ansible_tasks_config/ansible.cfg ANSIBLE_ROLES_PATH=$HOME/ansible_tasks_config/roles/ /usr/local/bin/ansible-playbook /var/lib/jenkins/ansible_tasks_config/letsencrypt-ssl.yml -i $HOME/ansible_tasks_config/inventory/inventory.txt
